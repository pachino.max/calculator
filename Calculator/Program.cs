﻿using System;
using CalculatorLibrary.Services;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculationService calculation = new CalculationService();
            calculation.InitializeComponents();
            calculation.InitializeOperation();
            calculation.Print();

            Console.ReadKey();
        }
    }
}
