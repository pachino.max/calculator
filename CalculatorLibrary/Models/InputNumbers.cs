﻿using System;
using System.ComponentModel.Design;

namespace CalculatorLibrary.Models
{
    public class InputNumbers
    {
        public double First { get; set; }

        public double Second { get; set; }

        public char Operation { get; set; }

        public InputNumbers()
        {
            Initialize();
        }

        private void Initialize()
        {
            Console.WriteLine("Введите первое число");
            First = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите знак операции");
            Operation = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Введите второе число");
            Second = Convert.ToDouble(Console.ReadLine());
        }
    }
}
