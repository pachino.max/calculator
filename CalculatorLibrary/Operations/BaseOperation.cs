﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using CalculatorLibrary.Models;

namespace CalculatorLibrary.Operations
{
    public abstract class BaseOperation
    {
        public double Result { get; set; }
        public InputNumbers Numbers { get; set; }
        protected BaseOperation(InputNumbers numbers)
        {
            Numbers = numbers;
        }

        public abstract void Calculate();
        public abstract void Print();
    }
}
