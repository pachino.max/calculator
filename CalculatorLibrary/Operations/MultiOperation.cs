﻿using System;
using System.Linq;
using System.Text;
using CalculatorLibrary.Models;

namespace CalculatorLibrary.Operations
{
    public class MultiOperation : BaseOperation
    {
        public MultiOperation(InputNumbers numbers) : base(numbers)
        {
        }
        public override void Calculate()
        {
            Result = Numbers.First * Numbers.Second;
        }

        public override void Print()
        {
            int spaces = Convert.ToInt32(RemovePointFromDouble(Numbers.Second)).ToString().Length + Convert.ToInt32(Numbers.First).ToString().Length;

            Console.WriteLine("\n");
            Console.WriteLine($"{new string(' ', spaces - Convert.ToInt32(RemovePointFromDouble(Numbers.First)).ToString().Length)}{Numbers.First}");
            Console.WriteLine($"{Numbers.Operation}");
            Console.WriteLine($"{new string(' ', spaces - Convert.ToInt32(RemovePointFromDouble(Numbers.Second)).ToString().Length)}{Numbers.Second}");
            Console.WriteLine(new string('-', spaces * 2));
            string multiplyNumber = Numbers.Second.ToString();
            if (multiplyNumber.Length>1)
            {


                multiplyNumber = RemovePointFromString(multiplyNumber);
                int innerSpaces = 0;
                for (var index = multiplyNumber.Length-1; index >= 0; index--)
                {
                    var num = multiplyNumber[index];
                    double value = double.Parse(num.ToString());
                    double result = value * RemovePointFromDouble(Numbers.First);
                    Console.WriteLine($"{new string(' ', spaces - Convert.ToInt32(RemovePointFromDouble(result)).ToString().Length-innerSpaces)}{result}");
                    innerSpaces++;
                    if (index!=0)
                    {
                        Console.WriteLine($"+");
                    }
                }
            }

            if (multiplyNumber.Length != 1)
            {
                Console.WriteLine(new string('-', spaces * 2));
            }
            Console.WriteLine($"{new string(' ', spaces - Convert.ToInt32(RemovePointFromDouble(Result)).ToString().Length)}{Result}");
        }

        private string RemovePointFromString(string removePoint)
        {
            var builder = new StringBuilder();
            foreach (var multi in removePoint)
            {
                if (multi != '.')
                {
                    builder.Append(multi);
                }
            }

            return builder.ToString();
        }

        private double RemovePointFromDouble(double removePoint)
        {
            var builder = new StringBuilder();

            foreach (var multi in removePoint.ToString())
            {
                if (multi != '.')
                {
                    builder.Append(multi);
                }
            }

            return Convert.ToDouble(builder.ToString());
        }
    }
}