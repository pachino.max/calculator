﻿using System;
using CalculatorLibrary.Models;

namespace CalculatorLibrary.Operations
{
    public class SubsOperation : BaseOperation
    {
        public SubsOperation(InputNumbers numbers) : base(numbers)
        {
        }

        public override void Calculate()
        {
            Result = Numbers.First - Numbers.Second;
        }

        public override void Print()
        {
            int spaces = Convert.ToInt32(Result).ToString().Length + Convert.ToInt32(Numbers.First).ToString().Length;

            Console.WriteLine("\n");

            Console.WriteLine("\n");
            Console.WriteLine($"{new string(' ', spaces - Convert.ToInt32(Numbers.First).ToString().Length)}{Numbers.First}");
            Console.WriteLine($"{Numbers.Operation}");
            Console.WriteLine($"{new string(' ', spaces - Convert.ToInt32(Numbers.Second).ToString().Length)}{Numbers.Second}");
            Console.WriteLine(new string('-', spaces * 2));
            Console.WriteLine($"{new string(' ', spaces - Convert.ToInt32(Result).ToString().Length)}{Result}");
        }
    }
}