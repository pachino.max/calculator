﻿using CalculatorLibrary.Models;
using CalculatorLibrary.Operations;

namespace CalculatorLibrary.Services
{
    public class CalculationService
    {
        public InputNumbers Numbers { get; private set; }

        public BaseOperation Operation { get; private set; }
        public void InitializeComponents()
        {
            Numbers = new InputNumbers();  
        }

        public void InitializeOperation()
        {
            switch (Numbers.Operation)
            {
                case '+':
                    Operation = new AddOperation(Numbers);
                    break;
                case '-':
                    Operation = new SubsOperation(Numbers);
                    break;
                case '*':
                    Operation = new MultiOperation(Numbers);
                    break;
                case '/':
                    Operation = new DivideOperation(Numbers);
                    break;
            }
        }

        public void Print()
        {
            Operation.Calculate();
            Operation.Print();
        }
    }
}
